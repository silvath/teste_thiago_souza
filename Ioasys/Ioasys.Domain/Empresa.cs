﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ioasys.Domain
{
    [Table("Empresa")]
    public class Empresa
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Cnpj { get; set; }

        public string Tipo { get; set; }
    }
}
