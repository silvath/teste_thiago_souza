﻿using Dapper;
using Dapper.Contrib.Extensions;
using Ioasys.Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace Ioasys.Data
{
    public class Repository
    {
        private readonly string dataDirectory = string.Empty;
        private readonly string stringConnection;
        private readonly string basePath;
        private readonly string pathDataBase;
        readonly IConfiguration configuration;

        public Repository()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(AppContext.BaseDirectory)
           .AddJsonFile($"appSettings.json");

            configuration = builder.Build();

            basePath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            pathDataBase = configuration.GetSection("PathDataBase").Value;
            dataDirectory = basePath + pathDataBase;
            stringConnection = string.Format($@"Server=(localdb)\mssqllocaldb; Integrated Security=true; AttachDbFileName={dataDirectory};");
        }

        public List<Empresa> ListarEmpresas()
        {
            List<Empresa> listaEmpresa = new List<Empresa>();

            using (IDbConnection db = new SqlConnection(stringConnection))
            {
                listaEmpresa = db.GetAll<Empresa>().ToList();
            }

            return listaEmpresa;
        }

        public Empresa ObterEmpresa(string nome)
        {
            Empresa empresa = new Empresa
            {
                Nome = nome,
            };

            using (IDbConnection db = new SqlConnection(stringConnection))
            {
                empresa = db.QueryFirst<Empresa>("Select Id as Id, Nome as Nome, Cnpj as Cnpj, Tipo as Tipo From Empresa where Nome = @nome", empresa);
            }

            return empresa;
        }

        public IEnumerable<Empresa> ObterEmpresaPorTipo(string tipo)
        {
            Empresa empresa = new Empresa
            {
                Tipo = tipo,
            };
            
            IEnumerable<Empresa> empresas;
            using (IDbConnection db = new SqlConnection(stringConnection))
            {
                empresas = db.Query<Empresa>("Select Id as Id, Nome as Nome, Cnpj as Cnpj, Tipo as Tipo From Empresa where Tipo = @tipo", empresa);
            }

            return empresas;
        }

        public Usuario ObterUsuario(string nome)
        {
            Usuario usuario = new Usuario
            {
                UserId = nome,
            };

            using (IDbConnection db = new SqlConnection(stringConnection))
            {
                usuario = db.QueryFirst<Usuario>("Select Id as Id, UsuarioId as UserId, Chave as AccessKey From Usuario where UsuarioId = @UserId", usuario);
            }

            return usuario;
        }

    }
}
