﻿using Ioasys.Data;
using Ioasys.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Service
{
    public class EmpresaService
    {
        private readonly Repository repository;

        public EmpresaService()
        {
            repository = new Repository();
        }
        public List<Empresa> ListarEmpresas()
        {
            var lista = repository.ListarEmpresas();

            return lista;
        }

        public Empresa ObterEmpresa(string nome)
        {
            return repository.ObterEmpresa(nome);
        }

        public Usuario ObterUsuario(string nome)
        {
            return repository.ObterUsuario(nome);
        }

        public IEnumerable<Empresa> ObterEmpresasPorTipo(string tipo)
        {
            return repository.ObterEmpresaPorTipo(tipo);
        }
    }
}
