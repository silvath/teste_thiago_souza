﻿using Ioasys.Data;
using Ioasys.Domain;
using Ioasys.Service;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Ioasys.Tests
{
    public class EmpresaTestes
    {
        private readonly EmpresaService empresaService;
        private readonly Repository repository;
        private readonly string dataDirectory = string.Empty;
        private readonly string stringConnection;
        private readonly string basePath;
        private readonly string pathDataBase;
        readonly IConfiguration configuration;

        public EmpresaTestes()
        {
            empresaService = new EmpresaService();
            repository = new Repository();

            var builder = new ConfigurationBuilder()
           .SetBasePath(AppContext.BaseDirectory)
           .AddJsonFile($"appsettings.json");

            configuration = builder.Build();

            basePath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            pathDataBase = configuration.GetSection("PathDataBase").Value;
            dataDirectory = basePath + pathDataBase;
            stringConnection = string.Format($@"Server=(localdb)\mssqllocaldb; Integrated Security=true; AttachDbFileName={dataDirectory};");
        }

        [Fact]
        public void ListarEmpresasTesteSucesso()
        {
            var empresas = repository.ListarEmpresas();
            Assert.IsType<List<Empresa>>(empresas);
        }

        [Fact]
        public void ObterEmpresasPorTipoTesteSucesso()
        {
            string tipo = "telecomunicações";
            var empresas = empresaService.ObterEmpresasPorTipo(tipo);
            Assert.IsType<List<Empresa>>(empresas);
        }

        [Fact]
        public void ObterEmpresasPorTipoTesteFalha()
        {
            string tipo = "";
            var empresas = empresaService.ObterEmpresasPorTipo(tipo);
            Assert.IsType<List<Empresa>>(empresas);
        }
    }
}
