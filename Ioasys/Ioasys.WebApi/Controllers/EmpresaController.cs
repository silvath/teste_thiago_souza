﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ioasys.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ioasys.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmpresaController : Controller
    {
        readonly EmpresaService empresaService;
        ILogger<string> log;

        public EmpresaController()
        {
            empresaService = new EmpresaService();
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
                    .AddConsole()
                    .AddEventLog();
            });
            log = loggerFactory.CreateLogger<string>();

        }

        public IActionResult Index()
        {
            return View();
        }

        [Produces("application/json")]
        [HttpGet("buscar")]
        public IActionResult Buscar()
        {

            string search = HttpContext.Request.Query["term"].ToString();
            var listaEmpresa = empresaService.ListarEmpresas();
            var names = listaEmpresa.Where(t => t.Nome.Contains(search)).Select(s => s.Nome).ToList();

            return Ok(names);
        }

        [HttpGet]
        [Route("MostrarEmpresa")]
        public JsonResult MostrarEmpresa(string nome)
        {
            try
            {

                if (nome != null)
                {
                    log.LogInformation($"Obtendo empresa do banco de dados");
                    var empresa = empresaService.ObterEmpresa(nome);

                    return Json(new { EmpresaViewModel = empresa });
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Ocorreu um erro ao buscar empresa no banco de dados: {ex}");
                return null;
            }
        }
        
        [HttpGet]
        [Route("ListarEmpresas")]
        public JsonResult ListarEmpresas()
        {
            try
            {

                log.LogInformation($"Obtendo empresas do banco de dados");
                var empresa = empresaService.ListarEmpresas();

                return Json(new { EmpresaViewModel = empresa });

            }
            catch (Exception ex)
            {
                log.LogError($"Ocorreu um erro ao buscar empresas no banco de dados: {ex}");
                return null;
            }
        }

        [HttpGet]
        [Route("ObterEmpresasPorTipo")]
        public JsonResult ObterEmpresasPorTipo(string tipo)
        {
            try
            {

                if (tipo != null)
                {
                    log.LogInformation($"Obtendo empresas do banco de dados");
                    var empresa = empresaService.ObterEmpresasPorTipo(tipo);

                    return Json(new { EmpresaViewModel = empresa });
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.LogError($"Ocorreu um erro ao buscar empresas no banco de dados: {ex}");
                return null;
            }
        }
    }
}